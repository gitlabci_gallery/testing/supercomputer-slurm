#!/bin/bash
# run.sh : submit slurm jobs and wait for completion before exiting
set -x

# to avoid a lock during fetching branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

# execution commands
salloc --nodes=2 --constraint=$CONS --time=00:01:00 -- mpiexec IMB-MPI1 PingPong 2>&1 |tee pingpong\_$CONS.out

# clean tmp
rm -rf /tmp/guix-$$

#!/bin/bash
# run.sh : execute script on MODE variable
set -x
[ -z "$CONS" ] && export CONS=bora
scriptdir=`dirname $PWD/$0`
$scriptdir/run_$MODE.sh

#!/bin/bash
# run.sh : submit slurm jobs and wait for completion before exiting

set -x

# to avoid a lock during fetching branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

JOB_NAME=pingpong\_$CONS
NP=2
TIME=00:01:00

# execution commands
sbatch --wait --job-name="$JOB_NAME" --output="$JOB_NAME.out" --nodes=$NP --constraint $CONS --time=$TIME ./test/pingpong.sl &

# ask bash to wait for the completion of all background commands
wait

# clean tmp guix
rm -rf /tmp/guix-$$
